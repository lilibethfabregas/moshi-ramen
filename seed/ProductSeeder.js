const Product = require('../models/products');

const mongoose = require('mongoose');

// Establish connection to the database
mongoose.connect('mongodb+srv://lilibethfabregas:batchoy08@cluster0-dyxdu.mongodb.net/gameon?retryWrites=true&w=majority', {useNewUrlParser: true});
// mongoose.connect(config.databaseURL, { useNewUrlParser: true }); 


const products = [
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/kani-ramen-salad-225x150.jpg',
		title: 'Kani Ramen Salad',
		category: 'Main Dish',
		price: 10
	}),
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/Tantan_Abura_Soba-225x150.jpg',
		title: 'Tantan Abura Soba',
		category: 'Main Dish',
		price: 8
	}),	
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/Curry_Tantanmen_2-225x150.jpg',
		title: 'Curry Tantanmen',
		category: 'Main Dish',
		price: 9
	}),	
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/Tantanmen_2-225x150.jpg',
		title: 'Tantanmen',
		category: 'Main Dish',
		price: 7
	}),	
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/Spicy_Tuna_Tacos-225x150.jpg',
		title: 'Spicy Tuna Tacos',
		category: 'Side Dish',
		price: 2
	}),
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/side-dishes-pic-5-225x150.jpg',
		title: 'Katsu Gyozayaki',
		category: 'Side Dish',
		price: 5
	}),
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/side-dishes-pic-3-225x150.jpg',
		title: 'Gyoza',
		category: 'Side Dish',
		price: 4
	}),	
	new Product({
		imagePath: 'http://www.butamaruramen.com/wp-content/uploads/2017/11/side-dishes-pic-1-225x150.jpg',
		title: 'Ebi Mayo',
		category: 'Side Dish',
		price: 4
	}),
	new Product({
		imagePath: 'https://www.medicalnewstoday.com/content//images/articles/320/320669/whiskey-glass.jpg',
		title: 'Rum',
		category: 'Drinks',
		price: 5
	}),	
	new Product({
		imagePath: 'https://cdn.cdnparenting.com/articles/2018/07/361921454-H.jpg',
		title: 'Cola',
		category: 'Drinks',
		price: 2
	}),	
	new Product({
		imagePath: 'https://www.dairygoodness.ca/var/ezflow_site/storage/images/dairy-goodness/home/recipes/summer-of-shakes/14873650-1-eng-CA/summer-of-shakes_large.jpg',
		title: 'Shakes',
		category: 'Drinks',
		price: 2
	})
];


let seed = 0;
for (let i = 0; i < products.length; i++) {
	products[i].save(function(err, result) {
		seed++;
		if (seed === products.length) {
			exit();			
		}
	});
}

function exit() {
	mongoose.disconnect();
}
