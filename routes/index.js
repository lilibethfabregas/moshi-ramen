var express = require('express');
var router = express.Router();

const Cart = require('../models/cart');
const Product = require('../models/products');
const Order = require('../models/order');

router.get('/', function(req, res, next) {
	const successMsg = req.flash('success')[0];
	Product.find(function(err, docs) {
		let productChunks = [];
		let chunkSize = 3;
		for (let i = 0; i < docs.length; i+= chunkSize) {
			productChunks.push(docs.slice(i, i + chunkSize));
		}
		  res.render('shop/index', 
			  	{ 	titles: 'Sisters Closet',
			  		products: productChunks,
			  		successMsg: successMsg,
			  		noMessage: !successMsg
			  	});
	});
		
});

router.get('/menu', function(req, res, next) {
	const successMsg = req.flash('success')[0];
	Product.find(function(err, docs) {
		let productChunks = [];
		let chunkSize = 3;
		for (let i = 0; i < docs.length; i+= chunkSize) {
			productChunks.push(docs.slice(i, i + chunkSize));
		}

		//let cart = new Cart(req.session.cart);
		  res.render('shop/menu', 
			  	{ 	titles: 'Sisters Closet',
			  		products: productChunks,
			  		successMsg: successMsg,
			  		noMessage: !successMsg
			  		// ,
			  		// product: cart.generateArray(), totalPrice: cart.totalPrice
			  	});
	});
		
});


router.get('/add-to-cart/:id', function(req, res, next) {
	let productId = req.params.id;
	let cart = new Cart(req.session.cart ? req.session.cart : {items: {}})

	Product.findById(productId, function(err, product) {
		if (err) {
			return res.redirect('/');
		}
		cart.add(product, product.id)
		req.session.cart = cart;
		console.log(req.session.cart);
		res.redirect('/menu');
	})
});

router.get('/add/:id', function(req, res, next) {
	let productId = req.params.id;
	let cart = new Cart(req.session.cart ? req.session.cart : {})

	cart.addByOne(productId);
	req.session.cart = cart;
	res.redirect('/shopping-cart');

})

router.get('/reduce/:id', function(req, res, next) {
	let productId = req.params.id;
	let cart = new Cart(req.session.cart ? req.session.cart : {})

	cart.reduceByOne(productId);
	req.session.cart = cart;
	res.redirect('/shopping-cart');
})

router.get('/remove/:id', function(req, res, next) {
	let productId = req.params.id;
	let cart = new Cart(req.session.cart ? req.session.cart : {})

	cart.removeItem(productId);
	req.session.cart = cart;
	res.redirect('/shopping-cart');

})

router.get('/shopping-cart', function(req, res, next) {

	if (!req.session.cart) {
		return res.render('shop/shopping-cart', {products: null});
	}

	var cart = new Cart(req.session.cart);
	res.render('shop/shopping-cart', {products: cart.generateArray(), totalPrice: cart.totalPrice})
})

router.get('/checkout', isLoggedIn, function(req, res, next) {
	if (!req.session.cart) {
		return res.redirect('/shopping-cart');
	}
	var cart = new Cart(req.session.cart);
	var errMsg = req.flash('error')[0];
	res.render('shop/checkout', {total: cart.totalPrice, errMsg: errMsg, noError: !errMsg})
});

router.post('/checkout', isLoggedIn, function(req, res, next) {
	if (!req.session.cart) {
		return res.redirect('/shopping-cart');
	}

	var cart = new Cart(req.session.cart);

	// Set your secret key: remember to change this to your live secret key in production
	// See your keys here: https://dashboard.stripe.com/account/apikeys
	const stripe = require('stripe')(
		'sk_test_G8SIAB4EUxRm4E0shtCPGsAI00HHqYMkTp'
	);

		stripe.charges.create({
		amount: cart.totalPrice * 100,
	    currency: "usd",
	    source: req.body.stripeToken,
	    description: "Sample charge"

	}, function(err, charge) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('/checkout');			
		}

		let order = new Order({
			user: req.user,
			cart: cart,
			address: req.body.address,
			name: req.body.name,
			paymentId: charge.id
		})
		order.save(function(err, result) {
			req.flash('success', "Successfully bought product/s");
			req.session.cart = null;
			res.redirect('/menu');
		});
		
	})

})

module.exports = router;

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	req.session.oldUrl = req.url;
	res.redirect('/users/signin')
}